<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SliderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('contact', [ContactController::class, 'store']);
Route::get('posts', [PostController::class, 'index']);
Route::get('products', [ProductController::class, 'index']);
Route::get('product/{sku}', [ProductController::class, 'show']);
Route::get('sliders', [SliderController::class, 'index']);
Route::post('payment', [PaymentController::class, 'store']);

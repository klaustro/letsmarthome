<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/** ADMIN ROUTES */
Route::group(['middleware' => ['auth',], 'prefix' => 'admin'], function () {
    Route::get('/', [App\Http\Controllers\LeadController::class, 'index'])->name('admin');
    Route::get('/logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index'])->name('admin');

    Route::get('products', [App\Http\Controllers\ProductController::class, 'index'])->name('products');
    Route::get('product', [App\Http\Controllers\ProductController::class, 'create'])->name('product.create');
    Route::get('product/{sku}', [App\Http\Controllers\ProductController::class, 'show'])->name('product.show');
    Route::get('product/delete/{id}', [App\Http\Controllers\ProductController::class, 'delete'])->name('product.delete');
    Route::post('product', [App\Http\Controllers\ProductController::class, 'store'])->name('product.store');
    Route::put('product/{id}', [App\Http\Controllers\ProductController::class, 'update'])->name('product.update');

    Route::get('leads', [App\Http\Controllers\LeadController::class, 'index'])->name('leads');
    Route::post('lead', [App\Http\Controllers\LeadController::class, 'store'])->name('lead.store');
    Route::get('lead/{id}', [App\Http\Controllers\LeadController::class, 'show'])->name('lead.show');
    Route::put('lead/{id}', [App\Http\Controllers\LeadController::class, 'update'])->name('lead.update');
    Route::get('lead/delete/{id}', [App\Http\Controllers\LeadController::class, 'delete'])->name('lead.delete');

    Route::get('orders', [App\Http\Controllers\OrderController::class, 'index'])->name('leads');
    Route::get('order', [App\Http\Controllers\OrderController::class, 'create'])->name('order.create');
    Route::post('order', [App\Http\Controllers\OrderController::class, 'store'])->name('order.store');
    Route::get('order/{id}', [App\Http\Controllers\OrderController::class, 'show'])->name('order.show');
    Route::put('order/{id}', [App\Http\Controllers\OrderController::class, 'update'])->name('order.update');
    Route::get('order/delete/{id}', [App\Http\Controllers\OrderController::class, 'delete'])->name('order.delete');

    Route::get('customers', [App\Http\Controllers\CustomerController::class, 'index'])->name('customers');
    Route::get('customer', [App\Http\Controllers\CustomerController::class, 'create'])->name('order.create');
    Route::post('customer', [App\Http\Controllers\CustomerController::class, 'store'])->name('customer.store');
    Route::get('customer/{id}', [App\Http\Controllers\CustomerController::class, 'show'])->name('customer.show');
    Route::put('customer/{id}', [App\Http\Controllers\CustomerController::class, 'update'])->name('customer.update');
    Route::get('customer/delete/{id}', [App\Http\Controllers\CustomerController::class, 'delete'])->name('customer.delete');

    Route::get('payments', [App\Http\Controllers\PaymentController::class, 'index'])->name('payments');
    Route::get('payment', [App\Http\Controllers\PaymentController::class, 'create'])->name('order.create');
    Route::post('payment', [App\Http\Controllers\PaymentController::class, 'store'])->name('payment.store');
    Route::get('payment/{id}', [App\Http\Controllers\PaymentController::class, 'show'])->name('payment.show');
    Route::put('payment/{id}', [App\Http\Controllers\PaymentController::class, 'update'])->name('payment.update');
    Route::get('payment/delete/{id}', [App\Http\Controllers\PaymentController::class, 'delete'])->name('payment.delete');

    Route::get('switch-lang', [App\Http\Controllers\LanguageController::class, 'switchLang'])->name('switchLang');

    Route::get('/notify-leads', [App\Http\Controllers\LeadController::class, 'notifyLeads'])->name('instagramPosts');

    Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
});

Route::get('/instagram-feed', [App\Http\Controllers\PostController::class, 'instagramFeed'])->name('instagramPosts');


Route::get('{key}', function () {
    return view('welcome');
})->where('key', '.*');

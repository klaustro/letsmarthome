<?php

namespace App\Providers;

use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\ServiceProvider;
use Torann\GeoIP\Facades\GeoIP;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Paginator::useBootstrap();

        //Check for 'lang' cookie
        $cookie = Cookie::get('lang');

        //Get visitors IP
        $userIp = request()->ip();

        //Get visitors Geo info based on his IP
        $geo = GeoIP::getLocation($userIp);


        if($geo == null) {
            //Probably a localhost server, set language to english

            //set locale from cookie if exists
            if (!isset($cookie) && !empty($cookie)) {
                \App::setLocale($cookie);
                return;
            }

            \App::setLocale('es');
        }

        // //Get visitors country name
        $userCountry = $geo['country'];


        //Set language based on country name
        // You can add as many as you want
        $supportedLanguages = [
            'Venezuela' => 'es',
            'Colombia' => 'es',
            'Peru' => 'es',
            'Ecuador' => 'es',
            'Bolivia' => 'es',
            'Chile' => 'es',
            'Paraguay' => 'es',
            'Uruguay' => 'es',
            'Argentina' => 'es',
            'Spain' => 'es',
            'United States' => 'en',
            'Canada' => 'en',
            'India' => 'en',
        ];

        if (!empty($cookie)) {
            //User has manually chosen a lang. We set it
            \App::setLocale($cookie);
        } else {

            //Check country name in supportedLanguages array
            if (array_key_exists($userCountry, $supportedLanguages)) {
                //Get userCountry value(language) from array
                $preferredLang = $supportedLanguages[$userCountry];
                //Set language based on value
                \App::setLocale($preferredLang);
                setcookie('lang', $preferredLang, time() + (60 * 60 * 24));

            } else {
                //If user is visiting from an unsupported country, default to English
                \App::setLocale('en');
            }
        }
    }
}

<?php

use App\Models\Inventory;
use App\Models\Order;

function updateStockByOrder(Order $order)
{
    foreach ($order->orderDetail as $detail) {
        Inventory::create([
            'order_detail_id' => $detail->id,
            'product_id' => $detail->product_id,
            'quantity' => -$detail->quantity,
        ]);
    }
}

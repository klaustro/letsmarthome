<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['image', 'slug', 'provider_id'];

    protected $appends = ['caption'];

    public function row()
    {
        return $this->morphOne(Row::class, 'rowable');
    }

    protected function language()
    {
        $lang = Language::where('code', \Lang::locale())->first();
        return $lang ? $lang : Language::first();
    }

    /**
     * Get Post translations
     **/
    public function getTranslationsAttribute()
    {
        return $this->row->translations;
    }

    /**
     * Caption form Translation
     **/
    public function getCaptionAttribute()
    {
        $translations = $this->translations
            ->where('language_id', $this->language()->id)
            ->where('key', 'caption')
            ->first();

        if (! $translations) {
            $translations = $this->translations
                ->where('language_id', 1)
                ->where('key', 'caption')
                ->first();
        }

        return $translations->value;
    }
}

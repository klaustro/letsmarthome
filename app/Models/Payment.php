<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = ['transaction_id', 'amount', 'name', 'email', 'status', 'trace',];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getPayerAttribute()
    {
        return $this->name ? $this->name : $this->order->customer->name;
    }
}

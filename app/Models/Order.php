<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['customer_id', 'status', 'delivery_number', 'shipping_address', 'city' ,'state' ,'postal_code' ,'country' , 'tracking', 'comments'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function payment()
    {
        return $this->hasMany(Payment::class);
    }

    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function fee()
    {
        return $this->hasOne(FeeOrder::class);
    }

    //ATTRIBUTES

    public function getTotalAttribute()
    {
        $total = 0;
        foreach ($this->orderDetail as $detail) {
            $total += $detail->quantity * $detail->price;
        }

        return $total;
    }

    public function getPayedAttribute()
    {
        return $this->payment->sum('amount');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['cost','retail','price','sku',];

    protected $appends = ['translations', 'picture', 'stock'];

    /**
     * Get the row for the post.
     */
    public function row()
    {
        return $this->morphOne(Row::class, 'rowable');
    }

    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }

    public function inventory()
    {
        return $this->hasMany(Inventory::class);
    }

    protected function language()
    {
        $lang = Language::where('code', \Lang::locale())->first();
        return $lang ? $lang : Language::first();
    }

    /**
     * Get Post translations
     **/
    public function getTranslationsAttribute()
    {
        return $this->row->translations;
    }

    public function translationValue($key, $language_id)
    {
        $translation = $this->translations
            ->where('language_id', $language_id)
            ->where('key', $key)
            ->first();

        return $translation ? $translation->value : null;
    }

    /**
     * Title form Translation
     **/
    public function getTitleAttribute()
    {
        $translations = $this->translations
            ->where('language_id', $this->language()->id)
            ->where('key', 'title')
            ->first();

        if (! $translations) {
            $translations = $this->translations
                ->where('language_id', 1)
                ->where('key', 'title')
                ->first();
        }

        return $translations->value;
    }

    /**
     * Description form Translation
     **/
    public function getDescriptionAttribute()
    {
        $translations = $this->translations
            ->where('language_id', $this->language()->id)
            ->where('key', 'description')
            ->first();

        if (! $translations) {
            $translations = $this->translations
                ->where('language_id', 1)
                ->where('key', 'description')
                ->first();
        }

        return $translations->value;
    }

    /**
     * Other Info form Translation
     **/
    public function getOtherAttribute()
    {
        $translations = $this->translations
            ->where('language_id', $this->language()->id)
            ->where('key', 'other')
            ->first();

        if (! $translations) {
            $translations = $this->translations
                ->where('language_id', 1)
                ->where('key', 'other')
                ->first();
        }

        return $translations->value;
    }

    public function getPictureAttribute()
    {
        return $this->pictures && count($this->pictures) > 0  ? $this->pictures[0]->path : false;
    }

    public function getStockAttribute()
    {
        return $this->inventory ? $this->inventory->sum('quantity') : 0;
    }
}

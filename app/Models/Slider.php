<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;

    protected $fillable = ['image', 'mobile_image', 'url'];

    protected $appends = ['translations'];

    public function row()
    {
        return $this->morphOne(Row::class, 'rowable');
    }

    protected function language()
    {
        $lang = Language::where('code', \Lang::locale())->first();
        return $lang ? $lang : Language::first();
    }

    /**
     * Get Post translations
     **/
    public function getTranslationsAttribute()
    {
        return $this->row->translations;
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Lead extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = ['name', 'email', 'address', 'phone', 'contact', 'status', 'last_notified_at', 'contacted_by', 'comments'];

    public function scopeSearch($query, $target)
	{
		if ($target != "")
			$query->where('name', 'LIKE', "%$target%")->orWhere('email', 'LIKE', "%$target%");
	}

    //Scopes
    public function scopeLastNotified($query, $days)
	{
		if ($days > 0){

            $query->where( 'last_notified_at', '<=', Carbon::now()->subDays($days))->orWhere('last_notified_at', null);
        }

	}
}

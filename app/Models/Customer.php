<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = ['name', 'legal_name','rif','email','phone','address','city','contact',];

    public function getRifFormatedAttribute()
    {
        $rif = '';
        $arrayRif = str_split($this->rif);

        foreach ($arrayRif as $key => $char) {
         if ($key == 1 || $key == 9) {
             $rif .= '-';
            }
            $rif .= $char;

        }

        return $rif;
    }


}

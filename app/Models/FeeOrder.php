<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeeOrder extends Model
{
    use HasFactory;
    
    protected $fillable = ['order_id', 'fee_id'];

    public function fee()
    {
        return $this->belongsTo(Fee::class);
    }
    
    public function getNameAttribute() 
    {
        return $this->fee->name;
    }
    
    public function getAmountAttribute() 
    {
        return $this->fee->amount;
    }
}

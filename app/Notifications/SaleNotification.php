<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SaleNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from('jpalencia@letsmarthome.com', 'Juan Palencia')
                    ->subject('Bombillos Led Inteligentes')
                    ->greeting($notifiable->name)
                    ->line('Somos una empresa que brinda asesoría en domótica. Nos especializamos en la  importación y distribución de todo lo necesario para  la automatización de espacios.')
                    ->line('El presente correo tiene como finalidad presentar las bondades de nuestro producto de entrega inmediata: <b> Bombillo Led Inteligente WIFI Multicolor.')
                    ->line('Nuestros bombillos son de gran calidad y tienen un tiempo de vida garantizado de más de 40.000 horas continuas (+ 4 años). Son fáciles de configurar y top en ventas en Estados Unidos y Europa.')
                    ->line('Nuestros precios insuperables y nuestra asesoría  continua, tanto a nuestros clientes y, a la vez, a sus clientes finales, garantizan una relación comercial  que va más allá de sólo comprar un producto de calidad.')
                    ->line($this->getProductTable())
                    ->line('Gracias y esperamos su confirmación para comunicarnos pronto.')
                    ->line('Atentamente')
                    ->salutation($this->getSignTable());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    private function getSignTable()
    {
        return '<table class="x_-1046579508ze_tableView" cellpadding="2" cellspacing="2" border="1" style="font-size  :  10pt; font-family  :  Arial,   Helvetica,   sans-serif; border-collapse  :  collapse; border  :  none;">
        <tbody>
            <tr>
                <td style="width: 247px;">
                    <div>
                        <div>
                            <img src="https://www.letsmarthome.com/img/logo.png" width="233" height="74" style="">
                            <br>
                            <br>
                        </div>
                        <div>
                            <a href="https://www.letsmarthome.com">
                            </a>
                            <br>
                        </div>
                        <div>
                            <br>
                        </div>
                    </div>
                </td>
                <td style="width :  220px;">
                    <div>
                        <span class="font" style="font-family:arial, helvetica, sans-serif, sans-serif">
                            <span class="size" style="font-size:15px">
                                Juan Palencia
                            </span>
                        </span>
                        <br>
                    </div>
                    <div>
                        <span class="font" style="font-family:arial, helvetica, sans-serif, sans-serif">
                            <span class="size" style="font-size:15px">
                                Director Comercial
                            </span>
                        </span>
                        <br>
                    </div>
                    <div>
                        <a href="mailto:jpalencia@letsmarthome.com">
                            <span class="font" style="font-family:arial, helvetica, sans-serif, sans-serif">
                                <span class="size" style="font-size:15px">
                                    jpalencia@letsmarthome.com
                                </span>
                            </span>
                        </a>
                        <span class="font" style="font-family:arial, helvetica, sans-serif, sans-serif">
                            <span class="size" style="font-size:15px">
                            </span>
                        </span>
                        <br>
                    </div>
                    <div>
                        <a href="https://www.letsmarthome.com">
                            <span class="font" style="font-family:arial, helvetica, sans-serif, sans-serif">
                                <span class="size" style="font-size:15px">
                                    https://www.letsmarthome.com
                                </span>
                            </span>
                            <span class="size" style="font-size:16px">
                                <span class="font" style="font-family:tahoma, arial, helvetica, sans-serif, sans-serif">
                                </span>
                            </span>
                            <span class="font" style="font-family:tahoma, arial, helvetica, sans-serif, sans-serif">
                            </span>
                        </a>
                        <br>
                    </div>
                    <div>
                        <div>
                            <div>
                                <div style="margin-top: 10px;">
                                    <a style="text-decoration: none;" href="https://www.instagram.com/letsmarthome">
                                        <img style="margin-right: 5px;" height="24" width="24" src="https://www.letsmarthome.com/img/mail-instagram.png">
                                    </a>
                                    <a style="text-decoration: none;" href="https://www.facebook.com/Letsmarthome-102753418632193">
                                        <img style="margin-right: 5px;" height="24" width="24" src="https://www.letsmarthome.com/img/mail-facebook.png">
                                    </a>
                                    <a style="text-decoration: none;" href="https://twitter.com/letsmarthome">
                                        <img style="margin-right: 5px;" height="24" width="24" src="https://www.letsmarthome.com/img/mail-twitter.png">
                                    </a>
                                    <a style="text-decoration: none;" href="https://wa.me/+584149900552">
                                        <img style="" height="24" width="24" src="https://www.letsmarthome.com/img/mail-whatsapp.png">
                                    </a>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>';
    }

    private function getProductTable()
    {
        return '<table class="x_1608192414ze_tableView" cellpadding="2" cellspacing="2" border="1" style="font-size :  10pt; font-family :  Verdana,  Arial,  Helvetica,  sans-serif; width :  100%; border-collapse :  collapse; border :  1px solid black;">
            <tbody>
                <tr>
                    <td style="width: 107px;">
                        <div>
                            <div>
                                <div>
                                    <img style="" height="116" width="110" src="https://www.letsmarthome.com/img/smart-bulb-box.jpg">
                                    <br>
                                </div>
                                <div>
                                    <a href="'. url('/product/smart-bulb') .'">
                                    </a>
                                    <br>
                                </div>
                            </div>

                        </div>
                    </td>
                    <td style="width :  253px;">
                        <div>
                            <b>
                                <span class="highlight" style="background-color:transparent">
                                    <span class="colour" style="color:rgb(0, 0, 0)">
                                        <a href="'. url('/product/smart-bulb') .'">
                                            <b>
                                                <span class="font" style="font-family:Arial">
                                                    <span class="size" style="font-size:16px">
                                                        Bombillo Led Inteligente WIFI Multicolor
                                                    </span>
                                                </span>
                                            </b>
                                        </a>
                                    </span>
                                </span>
                            </b>
                            <br>
                        </div>
                        <div>
                            <b>
                                Precio al mayor:
                            </b>
                            6.60$
                            <br>
                        </div>
                        <div>
                            <b>
                                Compra Mínima:
                            </b>
                            12 unidades.
                            <br>
                        </div>
                        <div>
                            <b>
                                Formas de pago:
                            </b>
                            Efectivo, Zelle, Paypal, Transferencias Banesco, Banco Mercantil, Banco Provincial, Banco de Venezuela.
                            <br>
                        </div>
                        <div>
                            <b>
                                Disponibilidad Inmediata - Envío Gratis.
                            </b>
                        </div>
                        <div>
                            <a href="https://www.instagram.com/tv/CT0m-Chg4ss/?utm_medium=copy_link" target="_blank" rel="noopener noreferrer">
                                Ver Demostración
                            </a>
                        </div>                        
                    </td>
                </tr>
            </tbody>
        </table><br>';
    }
}

<?php

namespace App\Notifications;

use App\Models\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderNotification extends Notification
{
    use Queueable;

	private $payment;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Payment $payment)
	{
		$this->payment = $payment;
	}

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->line(__('notification.purchase_thanks'))
                ->greeting(__('notification.greeting'))
                ->line(__('notification.order_id') . ': ' . $this->payment->order->id)
                ->line($this->getItemsTable())
                ->action(__('notification.visit_us'), url('/'))
                ->salutation(__('notification.regards'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    private function getItemsTable()
    {
        $table = '<table class="table" cellpadding="2" cellspacing="2" border="1" style="border-collapse:  collapse; border: none; padding: 10px 15px;">';
        $table .= '<tbody><tr><th>ID</th><th></th><th>Name</th><th>Quantity</th><th>Price</th></tr>';

        foreach ($this->payment->order->orderDetail as $item) {

            $table .= '<tr><td>';
            $table .= $item->product->id;
            $table .= '</td><td class="image">';
            $table .= '<a href="' . url('/product/'.$item->product->sku) . '" target="_blank" rel="noopener noreferrer"><img src="' . url($item->product->picture) . '" alt="' . $item->product->title . '"></a>';
            $table .= '</td><td>';
            $table .= $item->product->title;
            $table .= '</td><td class="right">';
            $table .= $item->quantity;
            $table .= '</td><td class="right">';
            $table .= $item->price;
            $table .= '</td></tr>';
        }

        $table .= '<tr><td></td><td></td><td></td><th class="right">Total</th><th class="right">' . number_format($this->payment->amount, 2) . '</th></tr>';

        $table .= '</tbody></table><br><br>';

        return $table;
    }
}

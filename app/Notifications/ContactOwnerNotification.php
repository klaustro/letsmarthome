<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ContactOwnerNotification extends Notification
{
    use Queueable;
    private $contact;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('notification.contact') . ' - ' . $this->contact->name)
            ->greeting(__('notification.greeting'))
            ->line('Ha recibido un contacto de ' . $this->contact->name)
            ->line('Puede contactarlo por el correo ' . $this->contact->email . ' o por el teléfono ' . $this->contact->phone)
            ->line($this->contact->name . ' escribió : "' . $this->contact->comment . '"')
            ->action(__('notification.visit_us'), url('/'))
            ->salutation(__('notification.regards'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

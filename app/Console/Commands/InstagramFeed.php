<?php

namespace App\Console\Commands;

use App\Models\Post;
use App\Models\Row;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use InstagramScraper\Instagram;

class InstagramFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:instagram';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Instagram posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $instagram = new \InstagramScraper\Instagram();
        Instagram::disableProxy();
        $instagram->setRapidApiKey(env('RAPID_API_KEY'));
        $medias = $instagram->getMedias('letsmarthome', 35);
        $count = 0;

        foreach (array_reverse($medias) as $media) {

            $id = $media->getId();
            if (Post::where('provider_id', $id)->count() == 0) {
                $url = $media->getImageThumbnailUrl();
                $contents = file_get_contents($url);
                $imageUrl = $this->truncateFileName( substr($url, strrpos($url, '/') + 1));
                Storage::put($imageUrl, $contents);

                $folder = '/storage/';

                $post = new Post();
                $post->image = $folder . $imageUrl;
                $post->url = $media->getLink();
                $post->provider_id = $id;
                $post->save();

                $post->row()->save(new Row());

                $post->row->translations()->create([
                    'language_id' => 1,
                    'key' => 'caption',
                    'value' => $media->getCaption(),
                ]);
                $count++;
            }
        }

        \Log::info("$count Posts loaded");
        $this->info("$count Posts loaded");
    }

    public function truncateFileName($name)
    {
        $pos = strpos($name, '?');
        return substr($name, 0, $pos);
    }
}

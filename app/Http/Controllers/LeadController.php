<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Notifications\SaleNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LeadController extends Controller
{   
    public function index()
    {
        $leads = Lead::orderByDesc('id')->search(request()->target)->simplePaginate(10);

        $maxDays = env('MAX_DAYS_NOTIFICATIONS', 30);
        $allLeads = Lead::all();

        $total = $allLeads->count();

        $unNotifiedLeads = Lead::lastNotified($maxDays)
                                ->whereNotIn('status', ['customer', 'undelivered'])
                                ->whereNotNull('email')
                                ->get()
                                ->count();

        return view('admin.leads.index', compact('leads', 'total', 'unNotifiedLeads'));
    }

    public function store()
    {

        request()->validate([
            'email' => 'nullable|unique:App\Models\Lead,email|email',
            'name' => 'required',
        ]);

        $lead = new Lead();
        $this->saveLead($lead);

        return redirect()->back()->with('success', __('validation.lead_added', ['name' => $lead->name]));
    }

    public function update($id)
    {
        $lead = Lead::find($id);
        $this->saveLead($lead);

        return redirect('/admin/leads')->with('success', __('validation.lead_updated', ['name' => $lead->name]));
    }

    public function show($id) {
        $lead = Lead::find($id);
        return view('admin.leads.show', compact('lead'));
    }

    private function saveLead(Lead $lead)
    {
        $lead->name = request()->name;
        $lead->email = strtolower(request()->email);
        $lead->address = request()->address;
        $lead->phone = request()->phone;
        $lead->contact = request()->contact;
        $lead->status = request()->status ? request()->status : 'lead';
        $lead->contacted_by = request()->contacted_by ? request()->contacted_by : 'email';
        $lead->comments = request()->comments;

        $lead->save();
    }


    public function notifyLeads()
    {
        $maxDays = env('MAX_DAYS_NOTIFICATIONS', 30);
		$max_emails = env('MAX_EMAILS', 40);
        $leads = Lead::lastNotified($maxDays)
                    ->whereNotIn('status', ['customer', 'undelivered'])
                    ->whereNotNull('email')
                    ->get();
        $count = 0;
        $sendedToday = $this->emailsSendedToday();

        if ($sendedToday >= $max_emails) {
            return redirect('/admin/leads')->with('error', __('validation.notify_max_sended', ['total' => $max_emails]));
        }

        foreach ($leads as $lead) {
            try {
                if ($count + $sendedToday < $max_emails) {

                    $lead->notify(new SaleNotification());
                    $lead->last_notified_at = now();
                    $lead->save();
                    $count++;
                    if ($count%5 == 0 && $count < $leads->count()) {
                        sleep(0.5);
                    }
                }
                else {
                    return redirect('/admin/leads')->with('error', __('validation.notify_max_sended', ['total' => $max_emails]));
                }
            } catch (\Exception $e) {
                report($e);
            }
        }

        return redirect('/admin/leads')->with('success', __('validation.notify_sended', ['total' => $count]));
    }

    private function emailsSendedToday()
    {
        $today = Carbon::today();
        $leads = Lead::whereDate('last_notified_at', $today)->get();
        return $leads->count();
    }

    public function delete($id)
    {
        $lead = Lead::find($id);
        $name = $lead->name;
        $lead->destroy($id);

        return redirect('/admin/leads')->with('success', __('validation.deleted', ['name' => $name]));
    }
}

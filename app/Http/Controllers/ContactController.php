<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\User;
use App\Notifications\ContactNotification;
use App\Notifications\ContactOwnerNotification;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function store() 
    {        
        app()->setLocale(request()->lang);
        
        $contact = Contact::create(request()->except(['lang']));
        $contact->save();

        $contact->notify(new ContactNotification);
        
        $user = User::find(1);

        if($user) {
            $user->notify(new ContactOwnerNotification($contact));
        }

        
        return response()->json([
            'message' => __('notification.response'),
        ]);
    }
}

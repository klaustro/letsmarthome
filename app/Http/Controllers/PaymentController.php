<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\Payment;
use App\Notifications\OrderNotification;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::simplePaginate();

        if(request()->ajax()) {
            return $payments;
        }

        return view('admin.payments.index', compact('payments'));
    }

    public function create()
    {
        $payment = null;
        $orders = Order::where('status', '<>', 'PAYED')->get();
        return view('admin.payments.create', compact('orders',));
    }

    public function store()
    {
        if(!request()->ajax()) {
            request()->validate([
                'order_id' => 'required',
                'amount' => 'required',
                'status' => 'required',
                'type' => 'required',
            ]);
        }

        \Log::debug(request()->trace);

        $payment = new Payment();
        $payment->order_id = request()->order_id ? request()->order_id : $this->createOrder()->id;
        $payment->transaction_id = request()->transaction_id;
        $payment->amount = request()->amount;
        $payment->name = request()->name;
        $payment->email = request()->email;
        $payment->status = request()->status;
        $payment->trace = json_encode(request()->trace);
        $payment->type = request()->type ? request()->type : 'paypal';
        $payment->save();

        $message = __('validation.payment_created', ['name' => $payment->id]);

        $order = $payment->order;

        if(request()->ajax()) {

            $order->customer->notify(new OrderNotification($payment));
            return ['message' => $message];
        }

        if ($order->payed >= $order->total) {
            $order->status = 'PAYED';
            $order->save();
        }


        return redirect('/admin/payments')->with('success', $message);
    }

    public function show($id)
    {
        $payment = Payment::find($id);

        return view('admin.payments.show', compact('payment'));
    }

    public function delete($id)
    {
        $payment = Payment::find($id);
        $name = $payment->id;
        $payment->destroy($id);

        return redirect('/admin/payments')->with('success', __('validation.deleted', ['name ' => $name]));
    }

    private function createOrder()
    {
        if(request()->ajax()) {
            $shippingAddress = request()->trace['purchase_units'][0]['shipping']['address'];
            $line2 = isset($shippingAddress['address_line_2']) ? ' ' . $shippingAddress['address_line_2'] : '';
            $address = $shippingAddress['address_line_1'] . $line2;
        } else {
            $address = request()->customer['address'];
        }

        $order = new Order();
        $order->customer_id = $this->createCustomer()->id;
        $order->shipping_address = $address;
        $order->city = $shippingAddress['admin_area_2'];
        $order->state = $shippingAddress['admin_area_1'];
        $order->postal_code = $shippingAddress['postal_code'];
        $order->country = $shippingAddress['country_code'];
        $order->status = 'PAYED';

        $order->save();

        foreach (request()->items as $item) {
            $order->orderDetail()->create([
                'product_id' => $item['id'],
                'price' => $item['retail'],
                'quantity' => $item['quantity'],
            ]);
        }

        updateStockByOrder($order);

        return $order;
    }

    private function createCustomer()
    {
        if(request()->ajax()) {
            $email = request()->trace['purchase_units'][0]['payee']['email_address'];

            $fullShipping = request()->trace['purchase_units'][0]['shipping'];
            $name = $fullShipping['name']['full_name'];
            $shippingAddress = $fullShipping['address'];

            $line2 = isset($shippingAddress['address_line_2']) ? ' ' . $shippingAddress['address_line_2'] : '';
            $address = $shippingAddress['address_line_1'] . $line2 . ' ' . $shippingAddress['admin_area_2'] . ' ' . $shippingAddress['admin_area_1'] . ' ' . $shippingAddress['country_code'];
            $city = $shippingAddress['admin_area_2'];
        } else {
            $name = request()->customer['name'];
            $email = request()->customer['email'];
            $address = request()->customer['address'];
            $city = '';
        }

        $customer = Customer::where('email', $email)->first();

        if (!$customer) {
            $customer = new Customer();
            $customer->name = $name;
            $customer->email = $email;
            $customer->phone = request()->customer['phone'];
            $customer->address = $address;
            $customer->city = $city;

            $customer->save();
        }

        return $customer;
    }
}

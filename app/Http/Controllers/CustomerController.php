<?php

namespace App\Http\Controllers;

use App\Models\Customer;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::orderBy('id', 'DESC')->simplePaginate();
        return view('admin.customers.index', compact('customers'));
    }


    public function create()
    {
        $customer = null;
        return view('admin.customers.show', compact('customer'));
    }


    public function show($id)
    {
        $customer = Customer::find($id);
        return view('admin.customers.show', compact('customer'));
    }

    public function store()
    {
        request()->validate([
            'name' => 'required',
            'legal_name' => 'required',
            'rif' => 'required|unique:App\Models\Customer,rif',
            'email' => 'required|unique:App\Models\Customer,email|email:rfc',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
        ]);

        $customer = new Customer();

        $this->saveCustomer($customer);
        return redirect('admin/customers')->with('success', __('validation.customer_stored'));
    }

    public function update($id)
    {
        $customer = Customer::find($id);

        $this->saveCustomer($customer);
        return redirect()->back()->with('success', __('validation.customer_updated'));
    }

    public function delete($id)
    {
        $customer = Customer::find($id);
        $name = $customer->name;
        $customer->destroy($id);

        return redirect('/admin/customers')->with('success', __('validation.deleted', ['name' => $name]));

    }

    public function saveCustomer(Customer $customer)
    {
        $customer->name = request()->name;
        $customer->legal_name = request()->legal_name;
        $customer->rif = request()->rif;
        $customer->email = request()->email;
        $customer->phone = request()->phone;
        $customer->address = request()->address;
        $customer->city = request()->city;
        $customer->contact = request()->contact;

        $customer->save();
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Picture;
use App\Models\Product;
use App\Models\Row;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();

        if(request()->ajax()) {
            return $products;
        }

        return view('admin.products.index', compact('products'));
    }

    public function show($value)
    {

        if(request()->ajax()) {
            $product = Product::where('sku', $value)->first();
            return $product;
        }

        $product = Product::find($value);
        return view('admin.products.show', compact('product'));
    }

    public function create()
    {
        $product = null;
        return view('admin.products.show', compact('product'));
    }

    public function store()
    {
        // request()->validate([
        //     'file' => 'required|mimes:jpg,png,jepg|max:2048'
        // ]);
        $product = new Product();

        $this->fillProduct($product);

        $this->storeImage($product);

        $product->row()->save(new Row());

        $this->storeTranslations($product);

        return redirect('admin/products')->with('success', __('validation.product_stored'));
    }

    public function update($id)
    {
        $product = Product::find($id);
        $this->fillProduct($product);
        $this->storeTranslations($product);

        return redirect()->back()->with('success', __('validation.product_updated'));
    }

    private function fillProduct(Product $product)
    {
        $request = request()->except([
            '_token',
            '_method',
            'picture',
            'spTitle',
            'spDescription',
            'enTitle',
            'enDescription',
        ]);

        foreach ($request as $key => $value) {
            $product->$key = $value;
        }

        $product->save();
    }


    private function storeImage($product)
    {
        $file = request()->file('picture');

        if($file) {
            $fileName = time().'_'.$file->getClientOriginalName();
            $contents = file_get_contents($file);
            Storage::put($fileName, $contents);

            $product->pictures()->save(new Picture([
                'path' => '/storage/' . $fileName,
                'product_id' => $product->id,
            ]));
        }
    }

    private function storeTranslations(Product $product)
    {
        $product->row->translations()->create([
            'language_id' => 1,
            'key' => 'title',
            'value' => request()->spTitle,
        ]);
        $product->row->translations()->create([
            'language_id' => 1,
            'key' => 'description',
            'value' => request()->spDescription,
        ]);
        $product->row->translations()->create([
            'language_id' => 2,
            'key' => 'title',
            'value' => request()->enTitle,
        ]);
        $product->row->translations()->create([
            'language_id' => 2,
            'key' => 'description',
            'value' => request()->enDescription,
        ]);
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $name = $product->title;
        $product->pictures()->delete();
        $product->destroy($id);

        return redirect('/admin/products')->with('success', __('validation.deleted', ['name' => $name]));
    }
}

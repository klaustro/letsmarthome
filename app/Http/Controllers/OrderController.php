<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Customer;
use App\Models\Fee;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::with('orderDetail', 'customer')->orderBy('id', 'DESC')->simplePaginate();
        return view('admin.orders.index', compact('orders'));
    }

    public function create()
    {
        $order = null;
        $customers = Customer::all();
        $products = Product::all();
        $fees = Fee::all();
        $orders = Order::whereDate('created_at', Carbon::today())->get();
        $number =  date("Ymd") . '-' . $this->formatZeros(intval($orders->count() + 1));
        return view('admin.orders.create', compact('order', 'customers', 'products', 'fees', 'number'));
    }

    public function store()
    {
        request()->validate([
            'customer_id' => 'required',
            'status' => 'required',
            'product_id' => 'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);

        $order = new Order();
        $this->saveOrder($order);

        updateStockByOrder($order);

        return redirect('/admin/orders')->with('success', __('validation.order_created', ['name' => $order->id]));
    }

	/**
	 * Factura: ?bill=1
	 * Sin IVA: ?nofee=1
	 * Tasa de Cambio: ?rate=4.15
	 * Fecha: ?date=2021-10-19
	 */

    public function show($id)
    {
        
        $order = Order::with('orderDetail.product', 'customer')->find($id);
        $customers = Customer::all();

        $rate = request()->rate;        
		
        if ($rate) {
            $order->totalEx = $order->total * $rate;
            
            foreach ($order->orderDetail as &$detail) {
                $detail->priceEx = $detail->price * $rate;
            }
        }

		$companyId = isset(request()->company) ? request()->company : 1;
		$company = Company::find($companyId);

		$order->fee = isset(request()->nofee) ? false : $order->fee;

        return view('admin.orders.show', compact('order', 'customers', 'company'));
    }

    public function delete($id)
    {
        $order = Order::find($id);
        $name = $order->id;
        $order->destroy($id);

        return redirect('/admin/orders')->with('success', __('validation.deleted', ['name' => $name]));
    }

    private function saveOrder(Order $order)
    {
        $order->customer_id = request()->customer_id;
        $order->status = request()->status;
        $order->delivery_number = request()->delivery_number;
        $order->shipping_address = request()->shipping_address;
        $order->tracking = request()->tracking;
        $order->comments = request()->comments;
        $order->save();

        foreach (request()->product_id as $key=> $product) {
            $order->orderDetail()->create([
                'order_id' => $order->id,
                'product_id' => $product,
                'quantity' => request()->quantity[$key],
                'price' => request()->price[$key],
            ]);
        }

        if (request()->fee) {
            $order->fee()->create([
                'order_id' => $order->id,
                'fee_id' => request()->fee,
            ]);
        }
    }

    private function formatZeros($value)
    {
        $length = 3;
        $char = 0;
        $type = 'd';
        $format = "%{$char}{$length}{$type}"; // or "$010d";

        //store to a variable
        return sprintf($format, $value);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use InstagramScraper\Instagram;

class PostController extends Controller
{
    public function index()
    {
        return Post::orderByDesc('id')->simplePaginate(6);
    }

    public function instagramFeed()
    {
        if(request()->key != 'MJASHOUJCLHJSD-ALHJSDOHRTF555') {
            return "<h1>You shall no pass!</h1>";
        }
        $instagram = new \InstagramScraper\Instagram();
        Instagram::disableProxy();
        $instagram->setRapidApiKey(env('RAPID_API_KEY'));
        $medias = $instagram->getMedias(request()->user);
        $count = 0;

        foreach (array_reverse($medias) as $media) {

            $id = $media->getId();
            if (Post::where('provider_id', $id)->count() == 0) {
                $url = $media->getImageThumbnailUrl();
                $contents = file_get_contents($url);
                $imageUrl = $this->truncateFileName( substr($url, strrpos($url, '/') + 1));
                Storage::put($imageUrl, $contents);

                $folder = '/storage/';

                $post = new Post();
                $post->image = $folder . $imageUrl;
                $post->url = $media->getLink();
                $post->provider_id = $id;
                $post->save();

                $post->row()->save(new Row());

                $post->row->translations()->create([
                    'language_id' => 1,
                    'key' => 'caption',
                    'value' => $media->getCaption(),
                ]);
                $count++;
            }
        }

        return "<h2>$count Posts loaded</h2>";

    }

    public function truncateFileName($name)
    {
        $pos = strpos($name, '?');
        return substr($name, 0, $pos);
    }
}

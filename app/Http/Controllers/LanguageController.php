<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function switchLang ()
    {
        $language = request()->lang;
        if($language) {

            \App::setLocale($language);
            setcookie('lang', $language, time() + (60 * 60 * 24));
        }
        return redirect()->back();
    }
}

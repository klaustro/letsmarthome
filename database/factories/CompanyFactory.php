<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
			'name' => $this->faker->company,
			'rif' => $this->faker->numerify('J#########'),
			'address' => $this->faker->address,
			'address2' => $this->faker->city,
			'phone' => $this->faker->phoneNumber,
			'phone2' => $this->faker->phoneNumber,
        ];
    }
}

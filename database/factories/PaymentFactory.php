<?php

namespace Database\Factories;

use App\Models\Payment;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' => $this->faker->numberBetween(1, 4),
            'transaction_id' => $this->faker->numerify('AAJ#DDD#FF#######'),
            'amount' => $this->faker-> randomNumber(3),
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'status' => 'COMPLETED',
        ];
    }
}

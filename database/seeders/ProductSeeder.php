<?php

namespace Database\Seeders;

use App\Models\Picture;
use App\Models\Product;
use App\Models\Row;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            'bombillo' => [
                [
                    'lang' => 2,
                    'title' => 'Smart Bulb',
                    'description' => 'Smart Bulb WiFi',
                    'other' => 'E27 9W',
                    'image' => '/img/smart-bulb.png'
                ],
                [
                    'lang' => 1,
                    'title' => 'Bombillo Smart',
                    'description' => 'Bombillo Inteligente',
                    'other' => 'E27 9W',
                ],

            ],
            'plug'  => [
                [
                    'lang' => 2,
                    'title' => 'Smart Socket',
                    'description' => 'Smart Socket WiFi',
                    'other' => '120V',
                    'image' => '/img/smart-plug.png'
                ],
                [
                    'lang' => 1,
                    'title' => 'Enchufe Smart',
                    'description' => 'Enchufe Inteligente',
                    'other' => '120V',
                ],
            ],
            'ir' => [
                [
                    'lang' => 2,
                    'title' => 'IR Control',
                    'description' => 'Smart Control',
                    'other' => 'IR',
                    'image' => '/img/smart-control.png'
                ],
                [
                    'lang' => 1,
                    'title' => 'Control IR',
                    'description' => 'Control Inteligente',
                    'other' => 'Infrarojo',
                ],
            ]
        ];

        foreach ($products as $item) {

            foreach ($item as $key => $value) {
                if ($value['lang'] == 2) {

                    $product = Product::factory()->create([
                        'cost' => 5.05,
                        'retail' => 12,
                        'price' => 8.5,
                        'sku' => Str::slug($value['title']),
                    ]);

                    $product->pictures()->save(new Picture([
                        'path' => $value['image'],
                        'product_id' => $product->id,
                    ]));

                    $product->row()->save(new Row());
                }

                $product->row->translations()->create([
                    'language_id' => $value['lang'],
                    'key' => 'title',
                    'value' => $value['title'],
                ]);

                $product->row->translations()->create([
                    'language_id' => $value['lang'],
                    'key' => 'description',
                    'value' => $value['description'],
                ]);

                $product->row->translations()->create([
                    'language_id' => $value['lang'],
                    'key' => 'other',
                    'value' => $value['other'],
                ]);
            }
        }
    }
}

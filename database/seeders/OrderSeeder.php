<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Inventory;
use App\Models\Order;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = Customer::all();

        foreach ($customers as $key => $customer) {

            $order = new Order();
            $order->customer_id = $customer->id;
            $order->save();

            $order->orderDetail()->create([
                'product_id' => 1,
                'price' => $key == 2 ? 8 : 8.5,
                'quantity' => $key == 0 ? 24 : 12,
            ]);

            updateStockByOrder($order);
        }

    }
}

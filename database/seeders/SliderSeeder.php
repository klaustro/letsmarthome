<?php

namespace Database\Seeders;

use App\Models\Row;
use App\Models\Slider;
use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sliders = [
            'bulbs' => [
                [
                    'lang' => '1',
                    'image' => '/img/slide-1.jpg',
                    'mobile_image' => '/img/mobile-slide-1.jpg',
                    'title' => 'Tu iluminación',
                    'subtitle' => 'inteligente',
                ],
                [
                    'lang' => '2',
                    'title' => 'Your smart',
                    'subtitle' => 'light',
                ],
            ],
            'sockets' => [
                [
                    'lang' => '1',
                    'image' => '/img/smart-socket-slide.jpg',
                    'mobile_image' => '/img/smart-socket-slide-mobile.jpg',
                    'title' => 'Tu conexión',
                    'subtitle' => 'inteligente',
                    'class' => 'top-right-left small'
                ],
                [
                    'lang' => '2',
                    'title' => 'Your smart',
                    'subtitle' => 'plug',
                    'class' => 'top-right-left small'
                ],
            ],
            'control' => [
                [
                    'lang' => '1',
                    'image' => '/img/ir-control-slide.jpg',
                    'mobile_image' => '/img/ir-control-slide-mobile.jpg',
                    'title' => 'Tu control',
                    'subtitle' => 'inteligente',
                    'class' => 'top-right-left gray'
                ],
                [
                    'lang' => '2',
                    'title' => 'Your smart',
                    'subtitle' => 'control',
                    'class' => 'top-right-left gray'
                ],
            ],
            'asistant' => [
                [
                    'lang' => '1',
                    'image' => '/img/smart-asistant-slide.jpg',
                    'mobile_image' => '/img/smart-asistant-slide-mobile.jpg',
                    'title' => 'Tu Asistente',
                    'subtitle' => 'inteligente',
                    'class' => 'center-top',
                ],
                [
                    'lang' => '2',
                    'title' => 'Your smart',
                    'subtitle' => 'asistant',
                    'class' => 'center-top',
                ],
            ],
        ];

        foreach ($sliders as $item) {

            foreach ($item as $value) {

                if ($value['lang'] == 1) {

                    $slider = Slider::factory()->create([
                        'image' => $value['image'],
                        'mobile_image' => $value['mobile_image'],
                    ]);

                    $slider->row()->save(new Row());
                }

                $slider->row->translations()->create([
                    'language_id' => $value['lang'],
                    'key' => 'title',
                    'value' => $value['title'],
                ]);

                $slider->row->translations()->create([
                    'language_id' => $value['lang'],
                    'key' => 'subtitle',
                    'value' => $value['subtitle'],
                ]);

                $slider->row->translations()->create([
                    'language_id' => $value['lang'],
                    'key' => 'class',
                    'value' => isset($value['class']) ? $value['class'] : '',
                ]);

            }
        }
    }
}

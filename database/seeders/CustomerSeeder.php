<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            [
                'name' => 'GRUPO PLUS MARKET',
                'legal_name' => 'GRUPO PLUS MARKET',
                'rif' => 'J500783795',
                'email' => 'solobombillos@gmail.com',
                'phone' => '04141128181',
                'address' => 'CALLE CAPITOLIO EDIF BMX PISO PB LOCAL BMX URB EL MARQUES',
                'city' => 'Caracas',
                'contact' => 'Jesus Bigorra',
            ],
            [
                'name' => 'MULTISERVICIOS SHADDAI, C.A',
                'legal_name' => 'MULTISERVICIOS SHADDAI, C.A',
                'rif' => 'J405784814',
                'email' => 'solobombillos@gmail.com',
                'phone' => '04141128181',
                'address' => 'CALLE SUCRE EDIF MARIA CONSUELO PISO PB LOCAL 3 URB',
                'city' => 'Caracas',
                'contact' => 'Jesus Bigorra',
            ],
            [
                'name' => 'TRANSPORTE RIGA C.A.',
                'legal_name' => 'TRANSPORTE RIGA C.A.',
                'rif' => 'J295252240',
                'email' => '',
                'phone' => '04122605007',
                'address' => 'CALLE SUCRE EDIF MARIA CONSUELO PISO PB LOCAL 3 URB',
                'city' => 'Caracas',
                'contact' => 'Peter',
            ],
            [
                'name' => 'TIENDA GF CHACAO, C.A.',
                'legal_name' => 'TIENDA GF CHACAO, C.A.',
                'rif' => 'J412359142',
                'email' => 'solobombillos@gmail.com',
                'phone' => '04141128181',
                'address' => 'CALLE ELICE LOCAL PB NRO 03 Y 04 SECTOR CHACAO CARACAS',
                'city' => 'Caracas',
                'contact' => 'Jesus Bigorra',
            ],
        ];

        foreach ($customers as $customer) {
            Customer::create($customer);
        }
    }
}

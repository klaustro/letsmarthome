<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create();
        \App\Models\Language::factory()->create([
            'code' => 'Es',
            'name' => 'Español'
        ]);
        \App\Models\Language::factory()->create([
            'code' => 'En',
            'name' => 'English'
        ]);

        $this->call(ProductSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(LeadSeeder::class);

        $this->call(SupplierSeeder::class);
        $this->call(PurchaseSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(OrderSeeder::class);
        
		\App\Models\Fee::create([
            'name' => 'IVA',
            'amount' => 16
        ]);
		
		$this->call(CompanySeeder::class);

    }
}

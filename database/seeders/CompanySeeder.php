<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Company::factory()->create([
			'name' => 'ADEMIA ADMINISTRACION & IT , C.A',
			'rif' => 'J-41131208-8',
			'address' => 'AV URDANETA ESQUINA PLAZA ESPAÑA EDIF FONDO COMUN TORRE SUR PISO 16',
			'address2' => 'OF 16-B URB LA CANDELARIA CARACAS DISTRITO CAPITAL ZONA POSTAL 1010',			
			'phone' => '(0212) 5736017',
			'phone2' => '(0414) 9900552',
		]);
		
		Company::factory()->create([
			'name' => 'CORPORACION A&T 0305,C.A.',
			'rif' => 'J-29793581-9',
			'address' => 'AV FCO DE MIRANDA EDIF LOS CARIBES PISO 1 APT 1-B URB',
			'address2' => 'CHACAO CARACAS (CHACAO) MIRANDA ZONA POSTAL 1060',
			'phone' => '(0212) 5736017',
			'phone2' => '(0414) 9900552',
		]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Supplier;
use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supplier::create([
            'name' => 'Guangzhou Jiguang Lighting Co., Ltd.',
            'address' => 'CN,Guangdong,Guangzhou,4th Floor, No. 38, Longgang South Road, Nanling, Taihe Town, Baiyun District',
            'email' => 'irene@chinaprojg.com',
            'phone' => '0086-020-23323729',
            'contact' => 'Irene Zhou',
            'url' => 'http://www.chinaproled.com',
        ]);
    }
}

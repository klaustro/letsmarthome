<?php

namespace Database\Seeders;

use App\Models\Inventory;
use App\Models\Purchase;
use Illuminate\Database\Seeder;

class PurchaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchase = Purchase::create([
            'supplier_id' => 1,
        ]);

        $detail = $purchase->detail()->create([
            'product_id' => 1,
            'quantity' => 200,
        ]);

        Inventory::create([
            'purchase_detail_id' => $detail->id,
            'product_id' => $detail->product_id,
            'quantity' => 200,
        ]);
    }
}

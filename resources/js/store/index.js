import Vue from 'vue'
import Vuex from 'vuex'
import Navbar from './modules/Navbar'
import Contact from './modules/Contact'
import Social from './modules/Social'
import Slider from './modules/Slider'
import Product from './modules/Product'
import Post from './modules/Post'
import Cart from './modules/Cart'

Vue.use(Vuex)


export default new Vuex.Store({
	modules: {
        Navbar,
		Contact,
        Social,
        Slider,
        Product,
        Post,
        Cart,
	}
})

import axios from "axios"
import { reject } from "lodash"

export default {
    state: {
        sliders: null
    },
    getters: {
        sliders: state => {
            return state.sliders
        },
    },
    actions: {
        getSliders({state, commit}, payload) {
            return new Promise((resolved, rejected) => {
                axios.get('/api/sliders')
                    .then(response => {
                        commit('setSliders', response.data)
                        resolved()
                    })
                    .catch(error => {
                        console.log(error)
                        rejected()
                    })
            })
        },
    },
    mutations: {
        setSliders(state, payload) {
            state.sliders = payload
        }
    }
}

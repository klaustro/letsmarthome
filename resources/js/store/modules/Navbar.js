export default {
    state: {
        collapsed: false,
        manual: false,
        open: false,
        currentLink: 'Product'
    },
    getters: {
        collapsed: state => {
            return state.collapsed
        },
        manual: state => {
            return state.manual
        },
        open: state => {
            return state.open
        },
        currentLink: state => {
            return state.currentLink
        },
    },
    mutations: {
        setCollapsed(state, payload) {
            state.collapsed = payload
        },
        setManualCollapsed(state) {
            state.manual = true
        },
        setOpenMenu(state, payload) {
            state.open = payload
        },
        setCurrentLink(state, payload) {
            state.currentLink = payload
        },
    }
}

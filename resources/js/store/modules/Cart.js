export default {
    state: {
        items: [],
        show: false,
        message: null,
        loading: false,
    },
    getters: {
        cartItems: state => {
            return state.items
        },
        itemsCount: state => {
            return state.items.length == 0 ? 0 : state.items.reduce((count,item) => count + item.quantity, 0)
        },
        showCart: state => {
            return state.show
        },
        paymentMessage: state => {
            return state.message
        },
    },
	actions: {
		sendPayment({state, commit}, payload) {
			state.loading = true
			return new Promise((resolved, reject) => {

				axios.post('/api/payment', payload)
				.then(response => {
					resolved(response)
					commit('paymentSuccesfull', payload)
				})
				.catch(error => {
					let issue = error.response.data.errors
					reject(issue)
					state.loading = false
				})
			})
		}
	},
    mutations: {
        addToCart(state, payload) {
            let item = state.items.find(item => item.id == payload.id)
            if(!item) {
                payload.quantity = 1
                state.items.push(payload)
            } else {
                item.quantity++
            }
            localStorage.setItem('items', JSON.stringify(state.items))
        },
        removeFromCart(state, payload) {
            let item = state.items.find(item => item.id == payload.id)
            item.quantity--
            if (item.quantity <= 0) {
                let index = state.items.findIndex(item => item.id == payload.id)
                state.items.splice(index, 1)

                if (state.items.length == 0) {
                    state.show = false
                }
            }
            localStorage.setItem('items', JSON.stringify(state.items))
        },
        clearFromCart(state, payload) {
            let index = state.items.findIndex(item => item.id == payload.id)
            state.items.splice(index, 1)

            if (state.items.length == 0) {
                state.show = false
            }

            localStorage.setItem('items', JSON.stringify(state.items))
        },
        setItems(state, payload) {
            state.items = payload
        },
        showCart(state) {
            state.show = true
        },
        hideCart(state) {
            state.show = false
        },
        paymentSuccesfull(state, payload) {
            state.message = payload.message
            state.items = []
            localStorage.setItem('items', JSON.stringify(state.items))
        }
    },
}

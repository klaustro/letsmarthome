import axios from "axios"

export default {
    state: {
        posts: [],
        morePosts: true,
        loading: false
    },
    getters: {
        posts: state => {
            return state.posts
        },
        morePosts: state => {
            return state.morePosts
        },
        socialLoading: state => {
            return state.loading
        },
    },
    actions: {
        getPosts({ state, commit }, payload) {
            state.loading = true
            return new Promise((resolved, rejected) => {
                axios.get('/api/posts?page=' + payload)
                    .then(response => {
                        commit('setPosts', response.data)
                        resolved()
                        state.loading = false
                    })
                    .catch(error => {
                        console.log(error)
                        rejected()
                        state.loading = false
                    })
            })
        },
    },
    mutations: {
        setPosts(state, payload) {
            payload.data.forEach(post => {
                state.posts.push(post)
            });

            state.morePosts = payload.next_page_url != null
        },
        clearPosts(state) {
            state.posts = []
        }
    }
}

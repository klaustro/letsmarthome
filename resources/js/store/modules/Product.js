import axios from "axios"

export default {
    state: {
        products: null,
        product: null,
        loading: false,
    },
    getters: {
        products: state => {
            return state.products
        },
        product: state => {
            return state.product
        },
        productLoading: state => {
            return state.loading
        },
    },
    actions: {
        getProducts({state, commit}, payload) {
            state.loading = true
            return new Promise((resolved, rejected) => {
                axios.get('/api/products')
                    .then(response => {
                        commit('setProducts', response.data)
                        resolved()
                        state.loading = false
                    })
                    .catch(error => {
                        console.log(error)
                        rejected()
                        state.loading = false
                    })
            })
        },
        getProduct({commit}, payload) {
            return new Promise((resolved, rejected) => {
                axios.get('/api/product/' + payload)
                    .then(response => {
                        commit('setProduct', response.data)
                        resolved()
                    })
                    .catch(error => {
                        console.log(error)
                        reject()
                    })
            })
        },
    },
    mutations: {
        setProducts(state, payload) {
            state.products = payload
        },
        setProduct(state, payload) {
            state.product = payload
        },
        clearProduct(state) {
            state.product = null
        }
    }
}

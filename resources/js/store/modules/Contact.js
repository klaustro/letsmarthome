const axios = require('axios')

export default {
	state: {
		loading: false,
		message: null,
        quote: null,
	},
	getters: {
		loading: state => {
		 	return state.loading
		},
		message: state => {
			return state.message
		},
		quote: state => {
			return state.quote
		},
	},
	actions: {
		sendContact({state, commit}, payload) {
			state.loading = true
			return new Promise((resolved, reject) => {

				axios.post('/api/contact', payload)
				.then(response => {
					resolved(response)
					commit('setMessage', response.data.message)
				})
				.catch(error => {
					let issue = error.response.data.errors
					reject(issue)
					state.loading = false
				})
			})
		}
	},
	mutations: {
		setMessage(state, payload) {
			state.message = payload
			state.loading = false
		},
		setQuote(state, payload) {
			state.quote = payload
		}
	},
}

export const helpers = {

    methods: {
        getTranslation(model, key) {
            let lang = this.$i18n.locale == 'es' ? 1 : 2
            let translation = model.translations.find(translation => translation.key == key && translation.language_id == lang)
            if (!translation) {
                return ''
            }
            return translation.value
        },
        getKeyByValue(object, value) {
            return Object.keys(object).find(key => object[key] === value);
        },
        requestQuote() {
            let title = this.getTranslation(this.product, 'title')
            this.$store.commit('setQuote', this.$t('cart.requestMessage') + title)

            if (this.$route.hash != '#contact') {

                this.$router.push('/#contact')
            }
        }
    },
    computed: {
        isMobile() {
            return 768 > Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
        },
        year() {
            var d = new Date();
            var n = d.getFullYear();
            return n
        },
        social() {
            return this.$store.getters.social
        },
    }
}

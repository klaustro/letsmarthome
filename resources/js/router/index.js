import Vue from 'vue'
import Router from 'vue-router'
import {helpers} from '../mixins/helpers'
import Home from '../views/HomeComponent'
import Product from '../views/ProductComponent.vue'
import CheckoutComponent from '../views/CheckoutComponent.vue'
import PaymentResponseComponent from '../views/PaymentResponseComponent.vue'
import NotFoundComponent from '../views/NotFoundComponent.vue'

Vue.use(Router)

let router = new Router({
    mode: 'history',
    mixins: [helpers],
    routes: [
        {
            name: 'home',
            path: '/',
            component: Home
        },
        {
            name: 'product',
            path: '/product/:sku',
            component: Product
        },
        {
            name: 'checkout',
            path: '/checkout',
            component: CheckoutComponent
        },
        {
            name: 'paymentResult',
            path: '/payment-result',
            component: PaymentResponseComponent
        },
		{
			path: '*',
			name: 'notFound',
			component: NotFoundComponent,
		},
    ],
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;

        }

         if (to.hash) {
            var header = document.getElementsByClassName("header")
            var collapsed = header[0].clientHeight <= 44
            var isMobile = 768 > Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
            var offset = isMobile ? 0 : collapsed ? 44 : 205

            return { selector: to.hash, behavior: 'smooth', offset: { x: 0, y: offset }};
        }
    return { x: 0, y: 0 }
  },
})

export default router

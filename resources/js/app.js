require('./bootstrap');

window.Vue = require('vue').default;
import store from './store'
import router from './router'

Vue.component('index-component', require('./views/Index.vue').default);

//Translations
import VueI18n from 'vue-i18n'
Vue.use(VueI18n);

import { languages } from './lang/index.js'
import { defaultLocale } from './lang/index.js'

const messages = Object.assign(languages)
var i18n = new VueI18n({
	locale: defaultLocale,
	fallbackLocale: 'es',
	messages
})

//Scroll To
var VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo, {
    duration: 300,
})

import AOS from 'aos'

import VueCookies from 'vue-cookies';
Vue.use(VueCookies);

var numeral = require("numeral");

Vue.filter("numberFormat", function (value) {
	return numeral(value).format("0,0.00");
});

const app = new Vue({
    created() {
        AOS.init()
        const itemsString = localStorage.getItem('items')
        if(itemsString) {
            const itemsData = JSON.parse(itemsString)
            this.$store.commit('setItems', itemsData)
        }
    },
	i18n,
	el: '#app',
	store,
    router,
});

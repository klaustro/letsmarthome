import en from './en.json'
import es from './es.json'
import VueCookies from 'vue-cookies';

let lang = VueCookies.get('lang')
export const defaultLocale = lang ? lang : 'es'

export const languages = {
  en: en,
  es: es,
}

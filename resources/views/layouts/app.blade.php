<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/favicon/apple-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/favicon/apple-icon-60x60.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/favicon/apple-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/favicon/apple-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/favicon/apple-icon-114x114.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/favicon/apple-icon-120x120.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/favicon/apple-icon-144x144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicon/apple-icon-152x152.png') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicon/apple-icon-180x180.png') }}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('/favicon/android-icon-192x192.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/favicon/favicon-96x96.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('/favicon/manifest.json') }}">
	
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">	

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{asset('js/jquery.js')}}" crossorigin="anonymous"></script>
    <script src="{{asset('js/bootstrap.js')}}" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
    <style>
        .dropdown {
            margin: 4px 0 0 10px;
        }

        .lang-flag {
            width: 24px;
            height: auto;
        }

        .number {
            text-align: right;
        }
    </style>
    @stack('styles')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('/img/logo.png')}}" alt="" style="width: 50%">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item {{ Route::currentRouteName() == 'home' ? 'active' : '' }}">
                            <a class="nav-link" href="/admin">{{__('menu.home') }}<span class="sr-only">(current)</span></a>
                          </li>

                          <li>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="dropdownCrudButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: uppercase">
                                  CRUDS
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownCrudButton">
                                    <a class="nav-link" href="/admin/customers">{{ __('menu.customers') }}</a>
                                    <a class="nav-link" href="/admin/products">{{ __('menu.products') }}</a>
                                    <a class="nav-link" href="/admin/leads">{{ __('menu.leads') }}</a>
                                    <a class="nav-link notify-leads" href="#">{{ __('menu.notifyLeads') }}</a>
                                    <a class="nav-link" href="/admin/orders">{{ __('menu.orders') }}</a>
                                    <a class="nav-link" href="/admin/payments">{{ __('menu.payments') }}</a>
                                </div>
                              </div>
                          </li>
                          <li class="nav-item {{ Route::currentRouteName() == 'logs' ? 'active' : '' }}">
                            <a class="nav-link" href="/admin/logs">{{ __('menu.logs') }}</a>
                          </li>
                          @if (Lang::locale() == 'es')
                            <li class="nav-item active">
                                <a class="nav-link lang-link" href="/admin/switch-lang?lang=en">
                                    <img class="lang-flag" src="{{ asset('/img/en.png') }}" alt="English" title="English">
                                </a>
                            </li>
                            @else
                            <li class="nav-item active }}">
                                <a class="nav-link lang-link" href="/admin/switch-lang?lang=es">
                                    <img class="lang-flag" src="{{ asset('/img/es.png') }}" alt="Español" title="Español">
                                </a>
                            </li>
                          @endif

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('menu.login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('menu.register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('menu.logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
<script>
$(function(){
  $('.notify-leads').off().on('click', function(){

    if ( confirm('{{ __('validation.notify_question') }}') ) {
       window.location = '/admin/notify-leads'
    }
  })

  $('.delete-row').off().on('click', function(){
      var name = $(this).data('name')
      var url = $(this).data('url')

    if ( confirm('{{ __('validation.delete_question') }} ' + name) ) {
       window.location = url
    }
  })
});
</script>
@stack('scripts')

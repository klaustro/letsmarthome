<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ __('validation.delivery_order') }} #{{ $order->delivery_number }} </title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>

<body class="{{ isset(request()->bill) ? 'bill' : '' }}">
    <div class="container">
        <div class="wrapper">
            <div class="top">
                <h1 class="logo"><img src="{{ asset('img/color-logo.png')}}" alt="Logo"></h1>
                <div class="enterprise">
                    <h2>{{ $company->name }}</h2>
                    <p>RIF: {{ $company->rif }}</p>
                    <p class="address">{{ $company->address }}</p>
					@if (isset($company->address2))
						<p class="address">{{ $company->address2 }}</p>
					@endif
                    <p>{{ $company->phone }}</p>
					@if (isset($company->phone2))
						<p>{{ $company->phone2 }}</p>
					@endif
                </div>
            </div>
            <div class="middle">
                <h5 class="title">{{__('validation.delivery_order')}}</h5>
                <p class="pink">{{ __('validation.order_date')}}: {{ isset(request()->date) ? request()->date : $order->created_at->format('d-m-Y') }}</p>
            </div>

            <div class="info">
                <div class="col">
                    <h3>{{__('validation.customer')}}</h3>
                    <h4>{{ $order->customer->legal_name ? $order->customer->legal_name : $order->customer->name }}</h4>
                    <h3 style="text-transform: capitalize">{{__('validation.attributes.address')}}</h3>
                    <p class="address">{{ $order->shipping_address ? $order->shipping_address : $order->customer->address }}</p>
                </div>
                <div class="col paddingLeft">
                    <p><b>RIF</b></p>
                    <p>{{ $order->customer->rif_formated }}</p>
                    <p><b>{{__('validation.lead_contact')}}</b></p>
                    <p><p>{{ $order->customer->contact }}</p></p>
                </div>
                <div class="col">
                    @if ($order->delivery_number)
                        <p><b>{{ isset(request()->bill) ? __('validation.bill_number') : __('validation.delivery_number') }}</b></p>
                        <p>{{ $order->delivery_number }}</p>
                    @endif
                </div>
            </div>
            <div class="detail">
                <table>
                    <tr>
                        <th>{{ __('validation.attributes.description') }}</th>
                        <th class="center">{{ __('validation.order_quantity') }}</th>
                        <th class="right">{{ __('validation.order_price') }}</th>
                        <th class="right">Total</th>
                    </tr>
                    @foreach ($order->orderDetail as $item)
                        <tr>
                            <td>{{ $item->product->title }}</td>
                            <td class="center">{{ $item->quantity }}</td>
                            @if (!request()->rate)
                                <td class="right">{{ number_format($item->price,2,",",".") }}</td>
                                <td class="right">{{ number_format($item->quantity * $item->price,2,",",".") }}</td>
                            @else
                                <td class="right">{{ number_format($item->priceEx,2,",",".") }}</td>
                                <td class="right">{{ number_format($item->quantity * $item->priceEx,2,",",".") }}</td>                            
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <th class="right">{{ __('validation.subtotal') }}</th>
                        @if (!request()->rate)
                            <th class="right">{{ number_format($order->total,2,",",".") }}</th>
                        @else
                            <th class="right">{{ number_format($order->totalEx,2,",",".") }}</th>
                        @endif
                    </tr>
                    @if ($order->fee)
                        <tr>
                            <td></td>
                            <td></td>
                            <th class="right">{{ $order->fee->name }} {{ $order->fee->amount }}%</th>
                            @if (!request()->rate)
                                <th class="right">{{ number_format($order->total * $order->fee->amount/100,2,",",".") }}</th>
                            @else
                                <th class="right">{{ number_format($order->totalEx * $order->fee->amount/100,2,",",".") }}</th>
                            @endif                            
                        </tr>
                    @endif
                </table>
                @if ($order->fee)                    
                    @if (!request()->rate)
                        <h6 class="pink right">{{ number_format($order->total + ($order->total * $order->fee->amount / 100),2,",",".") }}</h6>
                    @else
                        <h6 class="pink right">{{ number_format($order->totalEx + ($order->totalEx * $order->fee->amount / 100),2,",",".") }}</h6>
                    @endif
                @else
                    @if (!request()->rate)
                        <h6 class="pink right">{{ number_format($order->total,2,",",".") }}</h6>
                    @else
                        <h6 class="pink right">{{ number_format($order->totalEx,2,",",".") }}</h6>
                    @endif                    
                @endif
            </div>
            <div class="bottom">
                <p>{{ __('validation.notes') }}</p>
                @if ($order->status == 'CONSIGNATED')
                    <p>{{ __('validation.order_consignated') }}</p>
                @endif
                @if ($order->comments)
                    <p>{{ $order->comments }}{{ isset(request()->rate) ? ' (' . number_format(request()->rate,2,",",".") . ')' : '' }}</p>
                @endif
                <br>
                <br>
                <p>{{ __('validation.recived_by') }}</p>
                <br>
                <p>{{ __('validation.received_date') }}</p>
            </div>
        </div>
    </div>
</body>
</html>
<style>

.right {
    text-align: right;
}
</style>
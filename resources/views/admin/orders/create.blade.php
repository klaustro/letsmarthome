@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (\Session::has('success'))
            <div class="alert alert-success">
                {!! \Session::get('success') !!}
            </div>
            @endif

            @if (\Session::has('error'))
            <div class="alert alert-danger">
                {!! \Session::get('error') !!}
            </div>
            @endif

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header"><h2>{{ $order ? __('validation.order') . ' #' . $order->id : __('validation.new_order') }}</h2></div>
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
                @endif

                <div class="card-body">
                    @if ($order)
                        <form>
                    @else
                        <form action="/admin/order" method="POST">
                        @csrf
                    @endif
                        <div class="row">
                            <div class="form-group col">
                                <label for="name">{{ __('validation.customer_name') }}</label>
                                <select class="form-control"  name="customer_id" id="customer_id">
                                    <option value="">{{__('validation.select_customer')}}</option>
                                    @foreach ($customers as $customer)
                                        @if ($order && $order->customer_id == $customer->id)
                                            <option value="{{ $customer->id }}" selected>{{ $customer->name }}</option>
                                        @else
                                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="status">{{ __('validation.order_status') }}</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="CREATED" @if (old('status') == 'CREATED' ) selected @endif>{{ __('validation.order_status_created') }}</option>
                                    <option value="CONSIGNATED" @if (old('status') == 'CONSIGNATED' ) selected @endif>{{ __('validation.order_consignated') }}</option>
                                </select>
                            </div>

                            <div class="form-group col">
                                <label for="fee">{{ __('validation.order_fee') }}</label>
                                <select class="form-control" name="fee" id="fee">
                                    <option value="" selected>{{ __('validation.no_fee') }}</option>
                                    @foreach ($fees as $fee)
                                        <option value="{{ $fee->id }}">{{ $fee->name }}</option>
                                    @endforeach                                                                        
                                </select>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="status">{{ __('validation.delivery_number') }}</label>
                                <input type="text" name="delivery_number" class="form-control" value="{{ $number }}">
                            </div>
                            <div class="form-group col">
                                <label for="status">{{ __('validation.tracking') }}</label>
                                <input type="text" name="tracking" class="form-control" value="{{ $order && $order->tracking ? $order->tracking : ''  }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shipping_address">{{ __('validation.order_address') }}</label>
                            <textarea class="form-control" name="shipping_address" id="shipping_address" rows="3">{{ $order ? $order->shipping_address : '' }}</textarea>
                        </div>

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="15%">{{ __('validation.order_item') }}</th>
                                    <th width="45%">{{ __('validation.order_product') }}</th>
                                    <th width="20%">{{ __('validation.order_quantity') }}</th>
                                    <th width="20%" class="number">{{ __('validation.order_price') }}</th>
                                </tr>
                            </thead>
                            <tbody class="detail__content">
                                @if ($order)
                                    @foreach ($order->orderDetail as $key => $detail)
                                    <tr>
                                        <td>{{ $key + 1}}</td>
                                        <td>{{ $detail->product->title }}</td>
                                        <td>{{ $detail->quantity }}</td>
                                        <td class="number">{{ $detail->price }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr class="top-input">
                                        <td>
                                            <button class="btn btn-secondary add-item">+</button>
                                        </td>
                                        <td>
                                            <select class="form-control" id="product">
                                                <option>{{ __('validation.select_product') }}</option>
                                                @foreach ($products as $product)
                                                    <option value="{{ $product->id }}" data-price="{{ $product->price }}">{{ $product->title }}</option>
                                                @endforeach
                                              </select>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" id="quantity" value="1">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control number" id="price">
                                        </td>
                                    </tr>
                                @endif
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <th>Total</th>
                                        <th class="total number">{{ $order ? number_format($order->total,2,",",".") : 0 }}</th>
                                    </tr>
                                </tfoot>
                            </tbody>
                        </table>

                        <div class="form-group">
                            <label for="comments">{{ __('validation.attributes.comments') }}</label>
                            <textarea class="form-control" id="comments" name="comments" rows="3"></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">{{ __('validation.submit') }}</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
let orderDetail = []
$(function(){
    handleAddItem()
    handleProductChange()
  });

  function handleAddItem() {

    $('.add-item').off().on('click', function(e){
        e.preventDefault()
        let $container = $('.detail__content')
        let id = $('#product option:selected').val()
        let quantity = $('#quantity').val()
        let price = $('#price').val()

        if(id != '' && quantity > 0 && price > 0) {

            $container.append('<tr><td><button class="btn btn-danger remove-item" data-id="' + id + '">-</button><input type="hidden" value="' + id  + '" name="product_id[]"></td><td>' +
            $('#product option:selected').text() + '</td><td>' +
            '<input name="quantity[]" type="hidden" class="form-control" value="' + quantity  + '">' + quantity + '</td><td class="number">' +
            '<input name="price[]" type="hidden" class="form-control" value="' + price + '">' + price + '</td><td>')

            orderDetail.push({
                product_id: $('#product option:selected').val(),
                name: $('#product option:selected').text(),
                price: $('#price').val(),
                quantity: $('#quantity').val(),
            })

            $('#product option:selected').remove()

            if($('#product option').length == 1) {
                $('.top-input').hide()
            }

            handleRemoveItem()
            calculeTotal()
        }
    })
  }

  function handleProductChange() {
    $('#product').off().on('change', function(){
        let price = $('#product option:selected').data('price')
        $('#price').val(price)
        $('#quantity').focus()
    })
  }

  function handleRemoveItem() {
        $('.remove-item').off().on('click', function(e){
            e.preventDefault()
            $container = $(this).parent().parent()
            let id = $(this).data('id')

            for (let index = 0; index < orderDetail.length; index++) {
                const item = orderDetail[index];
                if (item.product_id == id) {
                    $("#product").append('<option value="' + id + '" data-price="' + item.price + '">' + item.name + '</option>');
                    orderDetail.splice(index, 1)
                }
            }

            $container.remove()

            if($('#product option').length > 1) {
                $('.top-input').show()
            }

            calculeTotal()
        })
    }

    function calculeTotal() {
        let $total = $('.total')
        let total = 0;

        for (let index = 0; index < orderDetail.length; index++) {
            const item = orderDetail[index];
            total += item.quantity * item.price
        }
        $total.text(total)
    }
  </script>
@endpush

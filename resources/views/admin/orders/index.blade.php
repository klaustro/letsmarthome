@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>
                        {{ __('menu.orders') }}
                        <a title="New Order" href="/admin/order" class="float-right"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M13 2v9h9v2h-9v9h-2v-9h-9v-2h9v-9h2zm2-2h-6v9h-9v6h9v9h6v-9h9v-6h-9v-9z"/></svg></a>
                    </h2>
                </div>

                @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
                @endif

                @if (\Session::has('error'))
                <div class="alert alert-danger">
                    {!! \Session::get('error') !!}
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="card-body">

                    <ul class="list-group">
                        @foreach ($orders as $order)
                            <li class="list-group-item">
                                <b class="name">{{ $order->id }} -</b>
                                <a href="/admin/order/{{$order->id}}" target="_blank">
                                    {{ $order->customer->name }}
                                </a>
                                <b class="name">{{ number_format($order->total,2,",",".") }}</b>
                            </li>
                        @endforeach
                      </ul>
                      <br>
                      <div class="d-flex">
                            <div class="mx-auto">
                            {{ $orders->links() }}
                            </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <style>
        h2 span {
            font-size: 14px;
        }
        .icon {
            margin-right: 10px;
        }

        .check-icon {
            fill: green;
        }

        .form-inline {
            margin: 0 auto;
            text-align: center;
            justify-content: space-between;
        }

        .form-inline .btn {
            width: 25%;
        }

        .search {
            position: relative;
        }

        .search svg {
            position: absolute;
            top: 6px;
            right: 6px;

        }

        .total {
            font-size: 13px;
        }

        @media(max-width: 768px) {
            .form-inline {
                flex-flow: column;
            }

            .form-inline .btn {
                width: 100%;
            }

            .form-inline .form-group {
                width: 100%;
            }
        }
    </style>
@endsection



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ __('validation.payment_receipt') }} #{{ $payment->id }} </title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
</head>
<style>
    * {
        margin: 0;
        padding: 0;
        margin-block-start: 0;
        margin-block-end: 0;
        margin-inline-start: 0;
        margin-inline-end: 0;
    }

    .container {
        max-width: 750px;
        margin: 0 auto;
        font-family: Roboto, arial;
    }

    .wrapper {
        padding: 0 60px;
    }

    .logo {
        width: 27%;
        margin: 0 26% 0 0;
    }

    .logo img {
        margin: 0 auto;
        width: 100%;
    }

    .enterprise {
        margin-top: 26px;
    }

    h2 {
        color: #528CC7;
        font-size: 14px;
        font-weight: 700;
        margin-bottom: 6px;
    }

    .middle {
        margin-top: 8px;
    }

    h3 {
        font-size: 13px;
        margin-bottom: 8px;
    }

    h4 {
        color: #a09b9b;
        font-size: 12px;
        margin-bottom: 8px;
    }

    h5 {
        color: #528CC7  ;
        font-size: 29px;
        font-weight: 700;
    }

    h6 {
        font-size: 20px
    }

    p {
        color: #444242;
        font-size: 11px;
        line-height: 1.3;
        margin-bottom: 6px;
    }

    b {
        font-weight: 700;
        font-size: 12px
    }

    .pink {
        font-weight: 700;
        color: #DE106E;
    }

    .top {
        bpayment-top: 8px solid #528CC7;
    }

    .info {
        margin-top: 28px;
    }

    .col {
        width: 45%;
    }

    .paddingLeft {
        padding-left: 10%;
    }

    .top, .info {
        display: flex;
        -ms-justify-content: flex-start;
        justify-content: flex-start;
        -ms-align-items: flex-start;
        align-items: flex-start;
        -ms-align-content: flex-start;
        align-content: flex-start;
        flex-wrap: wrap;
        flex-direction: row;
    }

    table {
        width: 100%;
    }

    tr:nth-child(even) {
        padding: 10px;
        background-color: #F3F3F3;
        -webkit-print-color-adjust: exact;
    }

    th {
        text-align: left;
        color: #528CC7;
        font-weight: 700;
        text-transform: capitalize;
        font-size: 14px
    }

    td {
        font-size: 11px;
    }

    td, th {
        padding: 10px 10px 10px 0;
    }

    .center {
        text-align: center;
    }

    .right {
        text-align: right;
    }

</style>
<body>
    <div class="container">
        <div class="wrapper">
            <div class="top">
                <h1 class="logo"><img src="{{ asset('img/color-logo.png')}}" alt="Logo"></h1>
                <div class="enterprise">
                    <h2>ADEMIA ADMINISTRACION & IT , C.A</h2>
                    <p>RIF: J-41131208-8</p>
                    <p>AV URDANETA EDIFICIO FONDO COMÚN OFC 16-B</p>
                    <p>CARACAS</p>
                    <p>(0212) 5736017</p>
                    <p>(0414) 9900552</p>
                </div>
            </div>
            <div class="middle">
                <h5>{{__('validation.payment_receipt')}}</h5>
                <p class="pink">{{ __('validation.payment_date')}}: {{ $payment->created_at->format('d-m-Y') }}</p>
            </div>

            <div class="info">
                <div class="col">
                    <h3>{{__('validation.customer')}}</h3>
                    <h4>{{ $payment->payer }}</h4>
                    <p class="address">{{ $payment->order->shipping_address ? $payment->order->shipping_address : $payment->order->customer->address }}</p>
                </div>
                <div class="col paddingLeft">
                @if ($payment->order->customer->rif)
                    <p><b>RIF</b></p>
                    <p>{{ $payment->order->customer->rif_formated }}</p>
                @endif
                @if ($payment->order->customer->contact)
                    <p><b>{{__('validation.lead_contact')}}</b></p>
                    <p><p>{{ $payment->order->customer->contact }}</p></p>
                @endif
                </div>
            </div>
            <div class="detail">
                <table>
                    <tr>
                        <th>{{ __('validation.attributes.description') }}</th>
                        <th>{{ __('validation.payment_type') }}</th>
                        <th class="right">Total</th>
                    </tr>
                    <tr>
                        <td>{{ __('validation.payment_description', ['name' => $payment->payer]) }} #{{ $payment->order->delivery_number ? $payment->order->delivery_number :$payment->order->id}}</td>
                        <td>{{ __('validation.' . $payment->type) }}</td>
                        <td class="right">{{ number_format($payment->amount,2,",",".") }}</td>
                    </tr>

                    <tr>
                        <td></td>
                        <th class="right">{{ __('validation.subtotal') }}</th>
                        <th class="right">{{ number_format($payment->amount,2,",",".") }}</th>
                    </tr>
                </table>
                <h6 class="pink right">{{ number_format($payment->amount,2,",",".") }}</h6>
            </div>
            <div class="bottom">
            @if ($payment->trace)
                <p>{{ __('validation.notes') }}</p>
                <p>{{ $payment->trace }}</p>
            @endif
            </div>
        </div>
    </div>
</body>
</html>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header"><h2>{{ __('validation.new_payment') }}</h2></div>
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
                @endif

                <div class="card-body">
                    <form action="/admin/payment" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="order_id">{{ __('validation.order') }}</label>
                            <select class="form-control"  name="order_id" id="order_id">
                                <option value="">{{__('validation.select_order')}}</option>
                                @foreach ($orders as $order)
                                    @if (old('order_id') == $order->id)
                                        <option value="{{ $order->id }}" selected>{{ $order->customer->name }} {{ __('validation.order') }}: {{ $order->id }}</option>
                                    @else
                                        <option value="{{ $order->id }}">#{{ $order->id }} {{ $order->customer->name }} ${{ number_format($order->total - $order->payed,2,",",".")}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="amount">{{__('validation.attributes.amount')}}</label>
                            <input type="text" class="form-control" name="amount" id="amount" value="{{ old('amount') }}">
                        </div>

                        <div class="form-group">
                            <label for="status">{{ __('validation.attributes.status') }}</label>
                            <select class="form-control"  name="status" id="status">
                                <option value="">{{__('validation.select_status')}}</option>
                                <option value="completed" @if (old('status') == 'completed') selected @endif>{{ __('validation.completed') }}</option>
                                <option value="partial" @if (old('status') == 'partial') selected @endif>{{ __('validation.partial') }}</option>
                                <option value="in_process" @if (old('status') == 'in_process') selected @endif>{{ __('validation.in_process') }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="type">{{ __('validation.attributes.type') }}</label>
                            <select class="form-control"  name="type" id="type">
                                <option value="">{{__('validation.select_type')}}</option>
                                <option value="paypal" @if (old('type') == 'paypal') selected @endif>Paypal</option>
                                <option value="zelle" @if (old('type') == 'zelle') selected @endif>Zelle</option>
                                <option value="credit_card" @if (old('type') == 'credit_card') selected @endif>{{ __('validation.credit_card') }}</option>
                                <option value="cash" @if (old('type') == 'cash') selected @endif>{{ __('validation.cash') }}</option>
                                <option value="transfer" @if (old('type') == 'transfer') selected @endif>{{ __('validation.transfer') }}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="trace">{{ __('validation.attributes.notes') }}</label>
                            <textarea class="form-control" id="trace" name="trace" rows="3">{{ old('trace') }}</textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">{{ __('validation.submit') }}</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <style>
        label {
            text-transform: capitalize;
        }
    </style>
@endpush

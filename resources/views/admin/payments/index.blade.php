@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>
                        {{ __('menu.payments') }}
                        <a title="{{ __('validations.new_payment') }}" href="/admin/payment" class="float-right"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M13 2v9h9v2h-9v9h-2v-9h-9v-2h9v-9h2zm2-2h-6v9h-9v6h9v9h6v-9h9v-6h-9v-9z"/></svg></a>
                    </h2>


                </div>

                @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
                @endif

                @if (\Session::has('error'))
                <div class="alert alert-danger">
                    {!! \Session::get('error') !!}
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="card-body">
                    <ul class="list-group">
                        @foreach ($payments as $payment)
                            <li class="list-group-item">
                                <b class="name">{{ $payment->id }}- </b>
                                <a href="/admin/payment/{{$payment->id}}" target="_blank">{{$payment->payer}} <b>{{ $payment->amount }}</b></a>
                                <a title="delete" href="#" data-name="{{ $payment->id }}" data-url="/admin/payment/delete/{{$payment->id}}" class="float-right delete-row">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3 6v18h18v-18h-18zm5 14c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm5 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm5 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm4-18v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.315c0 .901.73 2 1.631 2h5.712z"/></svg>
                                </a>
                            </li>
                        @endforeach
                      </ul>
                      <br>
                      <div class="mx-auto">
                          {{ $payments->links() }}
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



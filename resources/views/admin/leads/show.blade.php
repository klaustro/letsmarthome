@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>{{ $lead ? $lead-> title : 'New lead' }}</h2></div>
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
                @endif

                <div class="card-body">
                    <form action="/admin/lead/{{$lead->id}}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                        <div class="form-group">
                            <label for="name">{{ __('validation.lead_name') }}</label>
                            <input type="text" class="form-control"  name="name" id="name" value="{{ $lead->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">{{ __('validation.lead_email') }}</label>
                            <input type="text" class="form-control"  name="email" id="email" value="{{ $lead->email }}">
                        </div>
                        <div class="form-group">
                            <label for="address">{{ __('validation.lead_address') }}</label>
                            <input type="text" class="form-control"  name="address" id="address" value="{{ $lead->address }}">
                        </div>
                        <div class="form-group">
                            <label for="phone">{{ __('validation.lead_phone') }}</label>
                            <input type="text" class="form-control"  name="phone" id="phone" value="{{ $lead->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="contact">{{ __('validation.lead_contact') }}</label>
                            <input type="text" class="form-control-file" name="contact" id="contact" value="{{ $lead->contact }}">
                        </div>
                        <div class="form-group">
                            <label for="last_notified_at">{{ __('validation.notified_at') }}</label>
                            <input type="text" class="form-control-file" name="last_notified_at" id="last_notified_at" value="{{ \Carbon\Carbon::createFromTimeStamp(strtotime($lead->last_notified_at))->toDayDateTimeString() }}" disabled>
                        </div>

                        <div class="form-group">                            
                            <label class="" for="status">{{ __('validation.status') }}</label>
                            <select class="form-control" name="status" id="status">
                                <option value="lead" @if ((old('status') == 'lead') || $lead && $lead->status == 'lead' ) selected @endif>{{ __('validation.lead') }}</option>
                                <option value="customer" @if ((old('status') == 'customer') || $lead && $lead->status == 'customer' ) selected @endif>{{ __('validation.customer') }}</option>
                                <option value="undelivered" @if ((old('status') == 'undelivered') || $lead && $lead->status == 'undelivered' ) selected @endif>{{ __('validation.undelivered') }}</option>
                            </select>                            
                        </div>

                        <div class="form-group">                            
                            <label class="" for="contacted_by">{{ __('validation.contacted_by') }}</label>
                            <select class="form-control" name="contacted_by" id="contacted_by">
                                <option value="email" @if ((old('contacted_by') == 'email') || $lead && $lead->contacted_by == 'email' ) selected @endif>{{ __('validation.attributes.email') }}</option>
                                <option value="phone" @if ((old('contacted_by') == 'phone') || $lead && $lead->contacted_by == 'phone' ) selected @endif>{{ __('validation.attributes.phone') }}</option>
                                <option value="whatsapp" @if ((old('contacted_by') == 'whatsapp') || $lead && $lead->contacted_by == 'whatsapp' ) selected @endif>{{ __('validation.attributes.whatsapp') }}</option>
                            </select>                            
                        </div>

                        <div class="form-group">
                            <label for="comments">{{ __('validation.attributes.comments') }}</label>
                            <textarea class="form-control" id="comments" name="comments" rows="3">{{ $lead->comments }}</textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">{{ __('validation.submit') }}</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

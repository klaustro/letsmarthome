@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>{{ $product ? $product-> title : 'New Product' }}</h2></div>
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
                @endif

                <div class="card-body">


                    @if ($product )
                        <form action="/admin/product/{{$product->id}}" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                    @else
                        <form action="/admin/product" method="POST" enctype="multipart/form-data">
                    @endif
                        @csrf
                    @if ($product && $product->picture)
                        <div class="media">
                            <img src="{{ $product ? $product->picture : '' }}" class="mr-3" alt="{{$product ? $product->title : ''}}" style="width: 50%;">
                            <div class="media-body">
                    @endif
                                <div class="form-group">
                                    <label for="sku">Product SKU</label>
                                    <input type="text" class="form-control"  name="sku" id="sku" value="{{ $product ? $product->sku : ''}}">
                                </div>
                                <div class="form-group">
                                    <label for="cost">Product Cost</label>
                                    <input type="number" class="form-control"  name="cost" id="cost" value="{{ $product ? $product->cost : ''}}">
                                </div>
                                <div class="form-group">
                                    <label for="retail">Product Retail price</label>
                                    <input type="number" class="form-control"  name="retail" id="retail" value="{{ $product ? $product->retail : ''}}">
                                </div>
                                <div class="form-group">
                                    <label for="retail">Product Price</label>
                                    <input type="number" class="form-control"  name="price" id="price" value="{{ $product ? $product->price : ''}}">
                                </div>
                    @if ($product && $product->picture)
                            </div>
                        </div>
                    @else
                            <div class="form-group">
                                <label for="picture">Product image</label>
                                <input type="file" class="form-control-file" name="picture" id="picture">
                            </div>
                    @endif

                        <h3>Translations</h3>
                          <div class="form-group">
                            <label for="sp-title">Titulo de producto</label>
                            <input
                                type="text"
                                class="form-control"
                                name="spTitle"
                                id="spTitle"
                                value="{{ $product ? $product->translationValue('title', 1) : '' }}">
                          </div>
                          <div class="form-group">
                            <label for="sp-description">Decripción de producto</label>
                            <textarea
                                class="form-control"
                                name="spDescription"
                                id="spDescription"
                                rows="5"
                            >{{ $product ? $product->translationValue('description', 1) : '' }}</textarea>

                          </div>

                          <div class="form-group">
                            <label for="en-title">Product title</label>
                            <input
                                type="text"
                                class="form-control"
                                name="enTitle"
                                id="enTitle"
                                value="{{ $product ? $product->translationValue('title', 2) : '' }}">
                          </div>
                          <div class="form-group">
                            <label for="en-description">Product description</label>
                            <textarea
                                class="form-control"
                                name="enDescription"
                                id="enDescription"
                                rows="5"
                            >{{ $product ? $product->translationValue('description', 2) : '' }}</textarea>

                          </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

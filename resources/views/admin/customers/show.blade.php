@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header"><h2>{{ $customer ? $customer->name : __('validation.new_customer') }}</h2></div>
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
                @endif

                <div class="card-body">
                @if ($customer )
                    <form action="/admin/customer/{{$customer->id}}" method="POST">
                    @method('PUT')
                @else
                    <form action="/admin/customer" method="POST">
                @endif
                    @csrf
                        <div class="form-group">
                            <label for="name">{{ __('validation.attributes.name') }}</label>
                            <input type="text" class="form-control"  name="name" id="name" value="{{ $customer ? $customer->name : old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="legal_name">{{ __('validation.attributes.legal_name') }}</label>
                            <input type="text" class="form-control"  name="legal_name" id="legal_name" value="{{ $customer ? $customer->legal_name : old('legal_name') }}">
                        </div>
                        <div class="form-group">
                            <label for="rif">RIF</label>
                            <input type="text" class="form-control"  name="rif" id="rif" value="{{ $customer ? $customer->rif : old('rif') }}">
                        </div>
                        <div class="form-group">
                            <label for="email">{{ __('validation.attributes.email') }}</label>
                            <input type="text" class="form-control"  name="email" id="email" value="{{ $customer ? $customer->email : old('email') }}">
                        </div>
                        <div class="form-group">
                            <label for="phone">{{ __('validation.attributes.phone') }}</label>
                            <input type="text" class="form-control"  name="phone" id="phone" value="{{ $customer ? $customer->phone : old('phone') }}">
                        </div>
                        <div class="form-group">
                            <label for="address">{{ __('validation.attributes.address') }}</label>
                            <textarea class="form-control" id="address" name="address" rows="3">{{ $customer ? $customer->address : old('address') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="city">{{ __('validation.attributes.city') }}</label>
                            <input type="text" class="form-control" name="city" id="city" value="{{ $customer ? $customer->city : old('city') }}">
                        </div>
                        <div class="form-group">
                            <label for="contact">{{ __('validation.attributes.contact') }}</label>
                            <input type="text" class="form-control" name="contact" id="contact" value="{{ $customer ? $customer->contact : old('contact') }}">
                        </div>

                        <button type="submit" class="btn btn-primary">{{ __('validation.submit') }}</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <style>
        label {
            text-transform: capitalize;
        }
    </style>
@endpush

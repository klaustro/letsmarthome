<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LetSmartHome</title>
		
		<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/favicon/apple-icon-57x57.png') }}">
		<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/favicon/apple-icon-60x60.png') }}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/favicon/apple-icon-72x72.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/favicon/apple-icon-76x76.png') }}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/favicon/apple-icon-114x114.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/favicon/apple-icon-120x120.png') }}">
		<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/favicon/apple-icon-144x144.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicon/apple-icon-152x152.png') }}">
		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicon/apple-icon-180x180.png') }}">
		<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('/favicon/android-icon-192x192.png') }}">
		<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon/favicon-32x32.png') }}">
		<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/favicon/favicon-96x96.png') }}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon/favicon-16x16.png') }}">
		<link rel="manifest" href="{{ asset('/favicon/manifest.json') }}">

        <meta name=description content="{{ __('meta.description') }}">
        <meta name="keywords" content="smart life, domotica, smart home, smart bulbs, luces wifi , bombillos led, bombillos wifi, hogar inteligente, smart lighting, iluminacion led, bombillos de colores, casa inteligente"/>
        <meta property=og:title content="{{ __('meta.home') }} | {{ __('meta.title') }}"/>
        <meta property=og:type content=website />
        <meta property=og:url content="{{ url('/') }}"/>
        <meta property=og:image content="{{ url('/') }}{{ __('meta.image') }}"/>
        <meta property=og:description content="{{ __('meta.description') }}"/>
        <meta itemprop=name content="{{ __('meta.home') }} | {{ __('meta.title') }}">
        <meta itemprop=description content="{{ __('meta.description') }}">
        <meta itemprop=image content="{{ url('/') }}{{ __('meta.image') }}">
        <meta name=twitter:card content=summary_large_image>
        <meta name=twitter:site content="@letsmarthome">
        <meta name=twitter:title content="{{ __('meta.home') }} | {{ __('meta.title') }}">
        <meta name=twitter:description content="{{ __('meta.description') }}">
        <meta name=twitter:image content="{{ url('/') }}{{ __('meta.image') }}">
        <meta name=twitter:creator content="@letsmarthome">
        <link href="{{ mix('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <index-component/>
        </div>
    </body>
</html>
<script src="{{ mix('js/app.js') }}"></script>

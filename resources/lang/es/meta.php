<?php
return [
    'title' => 'Letsmarthome',
    'home' => 'Inicio',
    'description' => 'Convierte tu casa u oficina en un espacio inteligente, controla tus luces, cafetera, televisor, aire acondicionado y otros dispositivos desde cualquier parte o simplemente con tu voz.',
    'image' => '/img/share-es.jpg',
];

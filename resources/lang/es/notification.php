<?php

return [
    'contact' => 'Contacto',
    'greeting' => 'Hola',
    'thanks' => 'Gracias por contactarnos',
    'visit_us' => 'Visítenos',
    'we_contact' => 'En breves momentos te contactaremos',
    'regards' => 'Saludos',
    'response' => 'Su contacto fue recibido satisfactoriamente, pronto lo contactaremos',
    'purchase_thanks' => 'Gracias por su compra, a continuación el detalle de la compra',
    'order_id' => 'Número de Orden',
];

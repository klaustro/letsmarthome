<?php
return [
    'home' => 'Inicio',
    'products' => 'Productos',
    'customers' => 'Clientes',
    'leads' => 'Prospectos',
    'logs' => 'Registros',
    'orders' => 'Órdenes',
    'login' => 'Iniciar Sesión',
    'register' => 'Registro',
    'logout' => 'Cerrar Sesión',
    'notifyLeads' => 'Notificar Prospectos',
    'payments' => 'Pagos',
];

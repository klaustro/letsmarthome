<?php

return [
    'contact' => 'Contact',
    'greeting' => 'Hello',
    'thanks' => 'Thanks for contact us',
    'visit_us' => 'Visit us',
    'we_contact' => 'We will contact you soon',
    'regards' => 'Regards',
    'response' => 'Your contact was sended succesfully, We will contact you soon',
    'purchase_thanks' => 'Thanks for your purchase, below the purchase detail',
    'order_id' => 'Order ID',
];

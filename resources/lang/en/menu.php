<?php
return [
    'home' => 'Home',
    'products' => 'Products',
    'customers' => 'Customers',
    'leads' => 'Leads',
    'orders' => 'Orders',
    'logs' => 'Logs',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'notifyLeads' => 'Notify to leads',
    'payments' => 'Payments',
];

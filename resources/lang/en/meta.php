<?php
return [
    'title' => 'Letsmarthome',
    'home' => 'Home',
    'description' => 'Turn your home or office into a smart space, control your lights, cafeteria, air conditioning and other devices from anywhere or simply with your voice.',
    'image' => '/img/share-en.jpg',
];
